#ifndef TEST_UTIL_H
#define TEST_UTIL_H

#include <vector>

int test_equal(const char * testname, double value, double target);
int test_greater(const char * testname, double value, double target);
int test_less(const char * testname, double value, double target);

#endif
