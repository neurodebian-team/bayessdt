#include "test_util.h"
#include <cstdio>
#include <string.h>

const char OK[10]   = "  [OK]  \n";
const char FAIL[10] = "[failed]\n";

bool approx_equal(double x, double y, double eps=1e-7) {
    return (x-y)*(x-y)<eps*eps;
}

int test_equal(const char * testname, double value, double target) {
    char message[80];
    int n = strlen(testname);
    int k;
    int out(0);
    for (k=0; k<80; k++) message[k] = ' ';
    for (k=0; k<n; k++) message[k+2] = testname[k];
    if (approx_equal(value,target))
	for (k=0; k<10; k++) message[k+70] = OK[k];
    else {
	for (k=0; k<10; k++) message[k+70] = FAIL[k];
	out += 1;
    }
    fprintf(stderr,message);
    return out;
}

int test_greater(const char * testname, double value, double target) {
    char message[80];
    int n = strlen(testname);
    int k;
    int out(0);
    for (k=0; k<80; k++) message[k] = ' ';
    for (k=0; k<n; k++) message[k+2] = testname[k];
    if (value >= target)
	for (k=0; k<10; k++) message[k+70] = OK[k];
    else {
	for (k=0; k<10; k++) message[k+70] = FAIL[k];
	out += 1;
    }
    fprintf(stderr,message);
    return out;
}

int test_less(const char * testname, double value, double target) {
    char message[80];
    int n = strlen(testname);
    int k;
    int out(0);
    for (k=0; k<80; k++) message[k] = ' ';
    for (k=0; k<n; k++) message[k+2] = testname[k];
    if ( value < target )
	for (k=0; k<10; k++) message[k+70] = OK[k];
    else {
	for (k=0; k<10; k++) message[k+70] = FAIL[k];
	out += 1;
    }
    fprintf(stderr,message);
    return out;
}
