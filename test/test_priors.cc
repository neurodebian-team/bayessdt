#include <iostream>
#include "../src/prior.h"
#include "test_util.h"

using namespace std;

int test_BetaPrior(void) {
    int failed (0);
    BetaPrior * beta = new BetaPrior(1.,1.); // Should be uniform on (0,1)

    failed += test_equal("BetaPrior 1",beta->pdf(-.5),0);
    failed += test_equal("BetaPrior 2",beta->pdf(.5),1);
    failed += test_equal("BetaPrior 3",beta->pdf(1.5),0);

    delete beta;
    return failed;
}

int test_GammaPrior(void) {
    int failed (0);
    GammaPrior * gamma = new GammaPrior(2.);

    failed += test_equal("GammaPrior 1", gamma->pdf(-1.),0);
    failed += test_greater("GammaPrior 2", gamma->pdf(1.),0);

    delete gamma;
    return failed;
}

int test_GaussianPrior(void) {
    int failed (0);
    GaussianPrior * gauss = new GaussianPrior(0,1.);

    failed += test_equal("GaussianPrior 1",gauss->pdf(-1.),0.24197072451914337);
    failed += test_equal("GaussianPrior 2",gauss->pdf(0.),0.3989422804014327);
    failed += test_equal("GaussianPrior 3",gauss->pdf(1.),0.24197072451914337);

    delete gauss;

    return failed;
}

int test_RelationalPriors(void) {
    int failed (0);
    GreaterPrior * greater = new GreaterPrior ( 0. );
    LessPrior *    less    = new LessPrior ( 0. );

    failed += test_equal("GreaterPrior 1", greater->pdf(-1.), 0 );
    failed += test_equal("GreaterPrior 2", greater->pdf(1.),  1 );
    failed += test_equal("LessPrior 1", less->pdf(-1.), 1 );
    failed += test_equal("LessPrior 2", less->pdf(1.),  0 );

    delete greater;
    delete less;

    return failed;
}

int main(int argc, char ** argv) {
    int failed (0);
    int tests  (0);
    failed += test_BetaPrior();        tests ++;
    failed += test_GammaPrior();       tests ++;
    failed += test_GaussianPrior();    tests ++;
    failed += test_RelationalPriors(); tests ++;

    cerr << "===============================================\n";
    cerr << "  " << failed << " failures in " << tests << " tests\n";
}
