#!/usr/bin/env python

import pylab as p
from numpy import *
import sys

def logmsg(m):
    "Quickly write a log message"
    sys.stderr.write(m)
    sys.stderr.flush()

def getHist(samples,nbins=20):
    """Determine histograms for all params and likelihood"""
    npar = samples.shape[1]-2
    H = zeros((npar+1,nbins),'d')
    B = zeros((npar+1,nbins),'d')
    for par in xrange(npar+1):
        H[par,:],B[par,:] = histogram(samples[:,par],nbins)
    H /= samples.shape[0] # To get relative frequencies
    return H,B

def plotHist(ax,H,B):
    """Plot histograms (except lastone for likelihood)"""
    npar = H.shape[0]-1
    for par in xrange(npar):
        # ax.step(B[par,:],H[par,:],where="pre")
        ax.plot(B[par,:],H[par,:])

def acf(samples,nsteps=20):
    """Determine autocorrelations"""
    npar = samples.shape[1]-1
    nsamples = samples.shape[0]
    C = zeros((npar,nsteps),'d')
    for tau in xrange(nsteps):
        for par in xrange(npar):
            C[par,tau] = corrcoef(samples[:nsamples-tau,par],samples[tau:,par])[0,1]
    return C

def plotacf(C):
    """Plot the autocorrelation functions"""
    npar = C.shape[0]
    h = 0.5/npar
    x = arange(C.shape[1])
    cols = ["b","g","r","c","m","k"]
    for k in xrange(npar):
        ax = p.axes([.77,.55-h*(k+1),.2,0.8*h],xticklabels="",yticklabels="",yticks=(-.2,.2,.5,1))
        ax.stem(x,C[k,:],linefmt=cols[k]+"-",markerfmt=cols[k]+".")
        p.grid(True)
    p.setp(ax,xticklabels=('0','5','10','15','20'),yticklabels=('-.2','.2','.5','1'))

def plotraw(samples):
    """Plot the raw samples"""
    npar = samples.shape[1]-1
    h = 0.5/npar
    cols = ["b","g","r","c","m","k"]
    for k in xrange(npar):
        if k<npar-1:
            ax = p.axes([.4,.55-h*(k+1),.3,.8*h],xticklabels="")
        else:
            ax = p.axes([.4,.55-h*(k+1),.3,.8*h])
        ax.plot(samples[:,k],cols[k])
        if k<npar-1:
            minest = samples[:,:npar-1].min()
            maxest = samples[:,:npar-1].max()
            p.ylim(minest,maxest) # Das geht noch besser
            p.setp(ax,yticks=mgrid[round(minest):round(maxest):3j])
        else:
            # Automatically determine ticks
            mindeviance = samples[:,k].min()
            maxdeviance = samples[:,k].max()
            d = mindeviance-maxdeviance
            m = 0.5*(mindeviance+maxdeviance)
            m = round(m/10)*10
            d = round(d/10)*10
            mn = m-d
            mx = m+d
            p.setp(ax,yticks=[mn,m,mx])

def plotintervals(samples,labels):
    """Plot the posterior intervals"""
    npar = samples.shape[1]-2
    ax = p.axes([.07,.05+.4/npar,.25,.5-.5/npar])
    for par in xrange(npar):
        ax.plot(p.prctile(samples[:,par],(2.5,50,97.5)),[-par,-par,-par],'-d')
    ax.plot([0,0],[0.5,-npar+.5],'k:')
    p.ylim(-npar+.5,.5)
    p.setp(ax,yticks=[-par for par in xrange(npar)],yticklabels=labels)
    minest = samples[:,:npar-1].min()
    maxest = samples[:,:npar-1].max()
    print minest,maxest
    p.xlim(minest,maxest)
    p.setp(ax,xticks=mgrid[round(minest):round(maxest+1)])

# TODO:
# - Labels
# - Label with the estimates and posterior intervals
# - Label with DIC
# - How to deal with acceptance rate?

if len(sys.argv)>1:
    fname = sys.argv[1]
else:
    fname = "test.out"

# samples = loadtxt(fname)
f = open(fname)
n = len(f.readline().split())-1
samples = fromfile(f,sep=" ")
samples.shape = (-1,n)

logmsg("Loaded %d samples of %d parameters\n" % samples.shape)
npar = samples.shape[1]-2
parnames = ["par%d" % (k+1) for k in xrange(npar)]
print parnames

# Burnin and thinning
burnin = 5000
thin = 100
samples = samples[burnin::thin,:]

H,B = getHist(samples)
plotHist(p.axes([.08,.6,.3,.3]),H,B)

C = acf(samples)
plotacf(C)
plotraw(samples)
plotintervals(samples,parnames)
# p.savefig("MCMC.png")
p.show()
