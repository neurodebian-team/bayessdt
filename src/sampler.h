/*
 * bayessdt -- bayesian inference for rating based ROC curves
 * Copyright (C) 2009  Ingo Fründ
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */


#ifndef SAMPLER_H
#define SAMPLER_H

#include "rng.h"
#include "prior.h"
#include "sdtmodels.h"

#include <vector>
#include <fstream>

/**
 * \brief MCMC sampler to draw samples from an unnormalized posterior
 *
 * The Metropolis-Hastings Sampler is able to draw samples from an unnormalized posterior
 * by constructing a Markov Chain that has the posterior distribution as its stationary
 * distribution. New samples are proposed from a normal distribution. This should change
 * in future releases.
 */
class MetropolisHastingsSampler {
	private:
		rng *rgen;
		std::vector<double> stepwidths;
		Model *M;
		std::vector<std::vector<double> > samples;
		std::vector<double> lsamples;
		std::vector<int> accept;
		int nparameters;
		int nsamples;
	public:
		MetropolisHastingsSampler(
				unsigned int,    /**< number of samples to be drawn */
				unsigned int,    /**< number of free parameters in the model */
				Model *          /**< the model itself. */
				);
		double sample();  /**< start sampling */
		double sample(const std::vector<double>&); /**< start sampling with a specified starting value */
		void printsamples(std::ofstream&); /**< write samples to a file */
		/**
		 * set stepwidths for different parameters */
		void setstep(
				int,   /**< index of the parameter of interest */
				double /**< stepwidth for the parameter (currently standard deviation of proposal)*/
				);
		void getsamplesarray(double*,double*,int*); /**< return all samples in a long array*/
};

#endif
