/*
 * bayessdt -- bayesian inference for rating based ROC curves
 * Copyright (C) 2009  Ingo Fründ
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */


#include <Python.h>
#include <Numeric/arrayobject.h>
#include <cstdio>
#include "sdtmodels.h"
#include "sampler.h"

#define GAUSSPRIOR 0
#define BETAPRIOR 1
#define GAMMAPRIOR 2
#define GREATERPRIOR 3
#define LESSPRIOR 4

static char mcmc_roc_doc[] =
"mcmc_roc ( nlevels, responses, model=\"equalvariance\", start=None, nsamples=200000, stepsizes=None )\n"
"\n"
"Use MCMC to estimate a signal detection model for a ROC based on rating responses.\n"
"\n"
":Parameters:\n"
"    nlevels        number of confidence levels on the rating scale\n"
"    responses      an array with 2 x nlevels values giving the response\n"
"                   counts in the following order: <S0> <S1> ... <Sn> <N0>\n"
"                   <N1> ... <Nn>. Where S and N indicate signal and noise\n"
"                   stimuli respectively, n is the number of levels on the\n"
"                   rating scale, and the numbers indicate the subject's\n"
"                   confidence in favor of a signal.\n"
"     model         signal detection model to be fitted. This should be one of\n"
"                     'equalvariance'   standard equal variance gaussian model\n"
"                     'gumbelsdt'       equal variance model with gumbel distribution\n"
"                     'mixturegauss'    mixture model based on normal distribution\n"
"                     'mixturegumbel'   mixture model based on gumbel distribution\n"
"     start         alternate starting value, default is the MAP estimate\n"
"     nsamples      number of mcmc samples to be drawn\n"
"     stepsizes     a sequence of stepsizes in the order (cstep,dstep,lmstep)\n"
"                   or a dictionary {'c':cstep,'d':dstep,'lm':lmstep}\n"
"     priors        a list of priors for the parameters in the sequence criterion1, ...\n"
"                   criterionN, d (,a)\n"
"\n"
":Output:\n"
"    samples,likelihoods,accept\n"
"    samples        an array of nsamples x nparameters mcmc samples\n"
"    likelihoods    an array of the associated likelihoods\n"
"    accept         an array indicating wether the samples were accepted or not\n";

static char map_roc_doc[] =
"map_roc ( nlevels, responses, model=\"equalvariance\" )\n"
"\n"
"Determine maximum a posteriori estimate of a signal detection model\n"
"\n"
":Parameters:\n"
"    nlevels        number of confidence levels on the rating scale\n"
"    responses      an array with 2 x nlevels values giving the response\n"
"                   counts in the following order: <S0> <S1> ... <Sn> <N0>\n"
"                   <N1> ... <Nn>. Where S and N indicate signal and noise\n"
"                   stimuli respectively, n is the number of levels on the\n"
"                   rating scale, and the numbers indicate the subject's\n"
"                   confidence in favor of a signal.\n"
"     model         signal detection model to be fitted. This should be one of\n"
"                     'equalvariance'   standard equal variance gaussian model\n"
"                     'gumbelsdt'       equal variance model with gumbel distribution\n"
"                     'mixturegauss'    mixture model based on normal distribution\n"
"                     'mixturegumbel'   mixture model based on gumbel distribution\n"
"\n"
":Output:\n"
"    theta_max\n"
"    a parameter vector that is close to the maximum a posteriori estimate.\n"
"    WARNING: In particular for mixed models, \"close to\" might still be very far.\n";

static char loglikelihood_doc[] =
"loglikelihood ( nlevels, responses, theta, model=\"equalvariance\" )\n"
"\n"
"Determine the loglikelihood of a particular parameter set\n"
"\n"
":Parameters:\n"
"    nlevels        number of confidence levels on the rating scale\n"
"    responses      an array with 2 x nlevels values giving the response\n"
"                   counts in the following order: <S0> <S1> ... <Sn> <N0>\n"
"                   <N1> ... <Nn>. Where S and N indicate signal and noise\n"
"                   stimuli respectively, n is the number of levels on the\n"
"                   rating scale, and the numbers indicate the subject's\n"
"                   confidence in favor of a signal.\n"
"    theta          the parameter vector at which the likelihood should be evaluated\n"
"    model          signal detection model to be fitted. This should be one of\n"
"                     'equalvariance'   standard equal variance gaussian model\n"
"                     'gumbelsdt'       equal variance model with gumbel distribution\n"
"                     'mixturegauss'    mixture model based on normal distribution\n"
"                     'mixturegumbel'   mixture model based on gumbel distribution\n"
"\n"
":Output:\n"
"    l              the likelihood of the model at parameters theta\n";

/*************************************************************************************
 * mcmc_roc function that puts it all together                                       *
 *************************************************************************************/

static PyObject* mcmc_roc ( PyObject* self, PyObject* args, PyObject* kwargs ) {
	int Nlevels;
	PyObject *pyresponses;
	PyObject *pyrcount;
	PyObject *pystart = Py_None;
	PyObject *pystep  = Py_None,*pykey;
	PyObject *pyprior = Py_None;
	// Not very efficient but easy
	int prior_id[400];
	double prior_par_block[500];
	double *prior_par[400];
	prior_par[0] = prior_par_block;

	char *modelname = "equalvariance";
	int Nsamples = 200000;
	double *stepwidthc;
	double stepwidthd = 0.1;
	double stepwidthlm = 0.05;
	Model *M;
	MetropolisHastingsSampler *S;
	int k,m; // Some helper variables
	double a;
	bool mixturesdt(false);
	PyArrayObject *samples, *likelihoods, *accept;
	int samplesdim[2];
	int Nparams;

	// Parse command line
	static char *kwlist[] = {
		"nlevels",
		"responses",
		"modelname",
		"start",
		"Nsamples",
		"stepsizes",
		"priors",
		NULL};
	if (!PyArg_ParseTupleAndKeywords( args, kwargs, "iO|sOiOO", kwlist,
				&Nlevels, &pyresponses,  // Obligatory
				&modelname, &pystart, &Nsamples, &pystep, &pyprior // Optional
				))
		return NULL;

		// extract responses
	std::vector<int> responses(2*Nlevels);
	if (PySequence_Check(pyresponses)) {
		// pyresponses provides sequence protocol

		m = PySequence_Size(pyresponses);

		if (m != 2*Nlevels) { // Sequence has the wrong length
			PyErr_Format(PyExc_ValueError,
					"Number of response counts should be twice the number of"
					" confidence levels but it isn't.");
			return NULL;
		}

		// Go through sequence
		for (k=0; k<m; k++) {
			pyrcount = PySequence_GetItem(pyresponses,k);
			if (PyNumber_Check(pyrcount)) // All numbers are treated as valid response counts
				responses[k] = PyInt_AsLong(pyrcount);
			else {
				PyErr_Format(PyExc_ValueError,
						"Response counts should be integer.");
				return NULL;
			}
			Py_DECREF(pyrcount);
		}

	} else { // Response counts are no sequence at all
		PyErr_Format(PyExc_ValueError,
				"Response counts should be a sequence.");
		return NULL;
	}

	// Select the correct model equation
	if (!strcmp(modelname,"equalvariance"))
		M = new Model(Nlevels,Nlevels,responses,&equalvariance);
	else if (!strcmp(modelname,"gumbelsdt"))
		M = new Model(Nlevels,Nlevels,responses,&gumbelsdt);
	else if (!strcmp(modelname,"mixturegauss")) {
		M = new Model(Nlevels+1,Nlevels,responses,&mixturegauss);
		mixturesdt = true;
	} else if (!strcmp(modelname,"mixturegumbel")) {
		M = new Model(Nlevels+1,Nlevels,responses,&mixturegumbel);
		mixturesdt = true;
	} else {
		PyErr_Format(PyExc_ValueError,
				"Invalid sdt model selected");
		return NULL;
	}

	stepwidthc = new double [Nlevels-1];
	if (pystep!=Py_None) {
		// We have an argument for pystep ~> stepsize setting
		if (PySequence_Check(pystep)) {
			// pyresponses provides sequence protocol

			m = PySequence_Size(pystep);

			if (m != 2+(int)mixturesdt) {
				PyErr_Format(PyExc_ValueError,
						"Number of step sizes should be %d for this model.",
						2+(int)mixturesdt);
				return NULL;
			}
			// Go through sequence
			for (k=0; k<Nlevels-1; k++)
				stepwidthc[k] = PyFloat_AsDouble(PySequence_GetItem(pystep,0));
			stepwidthd = PyFloat_AsDouble(PySequence_GetItem(pystep,1));
			if (mixturesdt)
				stepwidthlm = PyFloat_AsDouble(PySequence_GetItem(pystep,2));
		} else if (PyDict_Check(pystep)) {
			// We have a dictionary
			pykey = PyString_FromString("c");
			if (PyDict_Contains(pystep,pykey)) {
				pykey = PyDict_GetItem(pystep,pykey); // This is the content
				if (PySequence_Check(pykey)) {
					// Individual stepwidths
					for (k=0; k<Nlevels-1; k++)
						stepwidthc[k] = PyFloat_AsDouble(PySequence_GetItem(pykey,k));
				} else {
					stepwidthc[0] = PyFloat_AsDouble(pykey);
					for (k=1; k<Nlevels-1; k++)
						stepwidthc[k] = stepwidthc[0];
				}
			}
			pykey = PyString_FromString("d");
			if (PyDict_Contains(pystep,pykey))
				stepwidthd = PyFloat_AsDouble(PyDict_GetItem(pystep,pykey));
			pykey = PyString_FromString("a");
			if (PyDict_Contains(pystep,pykey))
				stepwidthlm = PyFloat_AsDouble(PyDict_GetItem(pystep,pykey));
			pykey = PyString_FromString("lm");
			if (PyDict_Contains(pystep,pykey))
				stepwidthlm = PyFloat_AsDouble(PyDict_GetItem(pystep,pykey));
		} else {
			// This doesn't make sense
			PyErr_Format(PyExc_ValueError,
					"Stepsizes argument should be either sequence of dictionary.");
			return NULL;
		}
	} else {
		for (k=0; k<Nlevels-1; k++)
			stepwidthc[k] = .05;
	}

	// Set priors
	Nparams = Nlevels + (mixturesdt?1:0);
	if (PySequence_Check(pyprior)) {
	    // pypriors provide sequence protocol
	    m = PySequence_Size(pyprior);
	    for (k=0; k<m; k++) {
		pykey = PySequence_GetItem(pyprior,k);
		if (!PySequence_Check(pykey)) return NULL;
		prior_id[k] = PyInt_AsLong(PySequence_GetItem(pykey,0));
		switch (prior_id[k]) {
		    case GAUSSPRIOR: case BETAPRIOR:
			prior_par[k][0] = PyFloat_AsDouble(PySequence_GetItem(pykey,1));
			prior_par[k][1] = PyFloat_AsDouble(PySequence_GetItem(pykey,2));
			prior_par[k+1] = prior_par[k] + 2;
			break;
		    case GREATERPRIOR: case GAMMAPRIOR: case LESSPRIOR:
			prior_par[k][0] = PyFloat_AsDouble(PySequence_GetItem(pykey,1));
			prior_par[k+1] = prior_par[k] + 1;
			break;
		}
	    }
	}
	for (k=0; k<Nparams; k++) {
	    switch (prior_id[k]) {
		case GAUSSPRIOR:
		    M->setPrior(k,(Prior*)new GaussianPrior(prior_par[k][0],prior_par[k][1]));
		    break;
		case BETAPRIOR:
		    M->setPrior(k,(Prior*)new BetaPrior(prior_par[k][0],prior_par[k][1]));
		    break;
		case GAMMAPRIOR:
		    M->setPrior(k,(Prior*)new GammaPrior(prior_par[k][0]));
		    break;
		case GREATERPRIOR:
		    M->setPrior(k,(Prior*)new GreaterPrior(prior_par[k][0]));
		    break;
		case LESSPRIOR:
		    M->setPrior(k,(Prior*)new LessPrior(prior_par[k][0]));
		    break;
		default:
		    return NULL;
	    }
	}

	// Now we have everything to set up the sampler
	S = new MetropolisHastingsSampler( Nsamples, M->getnpar(), M);

	// Set stepwidths
	for (k=0; k<Nlevels-1; k++)
		S->setstep(k,stepwidthc[k]);
	S->setstep(Nlevels-1,stepwidthd);
	if (mixturesdt) S->setstep(Nlevels, stepwidthlm);

	// MCMC sampling
	if (pystart==Py_None) 
		a = S->sample();
	else {
		if (PySequence_Check(pystart))
		{
			m = PySequence_Size(pystart);
			if ( m!= M->getnpar()) {
				PyErr_Format(PyExc_ValueError,
						"Expected %d parameters as starting values but got %d",M->getnpar(),m);
				return NULL;
			}
			std::vector<double> start(M->getnpar());
			for (k=0; k<M->getnpar(); k++) {
				pyrcount = PySequence_GetItem(pystart,k);
				if (PyNumber_Check(pyrcount))
					start[k] = PyFloat_AsDouble(pyrcount);
				else {
					PyErr_Format(PyExc_ValueError,
							"starting values should be numbers.");
					return NULL;
				}
				Py_DECREF(pyrcount);
			}
			// Starting values are complete ~> start sampling
			a = S->sample(start);
		} else { // starting values are no sequence at all
			PyErr_Format(PyExc_ValueError,
					"starting values should be a sequence or None.");
			return NULL;
		}
	}

	// Put the samples into the respective arrays
	samplesdim[0] = Nsamples; samplesdim[1] = M->getnpar();
	samples     = (PyArrayObject*) PyArray_FromDims(2,samplesdim,PyArray_DOUBLE);
	likelihoods = (PyArrayObject*) PyArray_FromDims(1,&Nsamples,PyArray_DOUBLE);
	accept      = (PyArrayObject*) PyArray_FromDims(1,&Nsamples,PyArray_INT);
	S->getsamplesarray((double*)samples->data,
			(double*)likelihoods->data,
			(int*)accept->data);

	pyrcount = Py_BuildValue("(OOO)", samples, likelihoods, accept);
	Py_DECREF(samples);
	Py_DECREF(likelihoods);
	Py_DECREF(accept);

	delete S;
	delete M;
	delete stepwidthc;

	return pyrcount;
}

/*************************************************************************************
 * map_roc function that puts it all together                                        *
 *************************************************************************************/

static PyObject* map_roc ( PyObject* self, PyObject* args, PyObject* kwargs ) {
	int Nlevels;
	PyObject *pyresponses;
	PyObject *pyrcount;
	PyObject *pyprior = Py_None,*pykey;
	char *modelname = "equalvariance";
	Model *M;
	int k,m; // Some helper variables
	bool mixturesdt(false);
	PyArrayObject *mapestimate;
	// Not very efficient but easy
	int prior_id[400];
	double prior_par_block[500];
	double *prior_par[400];
	prior_par[0] = prior_par_block;

	// Parse command line
	static char *kwlist[] = {"nlevels","responses","modelname","priors",NULL};
	if (!PyArg_ParseTupleAndKeywords( args, kwargs, "iO|sO", kwlist,
				&Nlevels, &pyresponses, &modelname, &pyprior ))
		return NULL;

	// extract responses
	std::vector<int> responses(2*Nlevels);
	if (PySequence_Check(pyresponses)) {
		// pyresponses provides sequence protocol

		m = PySequence_Size(pyresponses);

		if (m != 2*Nlevels) { // Sequence has the wrong length
			PyErr_Format(PyExc_ValueError,
					"Number of response counts should be twice the number of"
					" confidence levels but it isn't.");
			return NULL;
		}

		// Go through sequence
		for (k=0; k<m; k++) {
			pyrcount = PySequence_GetItem(pyresponses,k);
			if (PyNumber_Check(pyrcount)) // All numbers are treated as valid response counts
				responses[k] = PyInt_AsLong(pyrcount);
			else {
				PyErr_Format(PyExc_ValueError,
						"Response counts should be integer.");
				return NULL;
			}
			Py_DECREF(pyrcount);
		}

	} else { // Response counts are no sequence at all

		PyErr_Format(PyExc_ValueError,
				"Response counts should be a sequence.");
		return NULL;
	}

	// Select the correct model equation
	if (!strcmp(modelname,"equalvariance"))
		M = new Model(Nlevels,Nlevels,responses,&equalvariance);
	else if (!strcmp(modelname,"gumbelsdt"))
		M = new Model(Nlevels,Nlevels,responses,&gumbelsdt);
	else if (!strcmp(modelname,"mixturegauss")) {
		M = new Model(Nlevels+1,Nlevels,responses,&mixturegauss);
		mixturesdt = true;
	} else if (!strcmp(modelname,"mixturegumbel")) {
		M = new Model(Nlevels+1,Nlevels,responses,&mixturegumbel);
		mixturesdt = true;
	} else {
		PyErr_Format(PyExc_ValueError,
				"Invalid sdt model selected");
		return NULL;
	}

	// Set priors
	/*
	for (k=0; k<Nlevels-1; k++)
		M->setPrior(k,(Prior*)new GaussianPrior(k*dl,1e20));
	*/
	/*
	M->setPrior(0,(Prior*)new GaussianPrior(0,1e20));
	for (k=1; k<Nlevels-1; k++)
		M->setPrior(k,(Prior*)new GreaterPrior(0));
	M->setPrior(Nlevels-1,(Prior*)new GammaPrior(2));
	if (mixturesdt) M->setPrior(Nlevels,(Prior*)new BetaPrior(1.,1.));
	*/
	// Set priors
	int Nparams = M->getnpar();
	if (PySequence_Check(pyprior)) {
	    // pypriors provide sequence protocol
	    m = PySequence_Size(pyprior);
	    for (k=0; k<m; k++) {
		pykey = PySequence_GetItem(pyprior,k);
		if (!PySequence_Check(pykey)) return NULL;
		prior_id[k] = PyInt_AsLong(PySequence_GetItem(pykey,0));
		switch (prior_id[k]) {
		    case GAUSSPRIOR: case BETAPRIOR:
			prior_par[k][0] = PyFloat_AsDouble(PySequence_GetItem(pykey,1));
			prior_par[k][1] = PyFloat_AsDouble(PySequence_GetItem(pykey,2));
			prior_par[k+1] = prior_par[k] + 2;
			break;
		    case GREATERPRIOR: case GAMMAPRIOR: case LESSPRIOR:
			prior_par[k][0] = PyFloat_AsDouble(PySequence_GetItem(pykey,1));
			prior_par[k+1] = prior_par[k] + 1;
			break;
		}
	    }
	}
	for (k=0; k<Nparams; k++) {
	    switch (prior_id[k]) {
		case GAUSSPRIOR:
		    M->setPrior(k,(Prior*)new GaussianPrior(prior_par[k][0],prior_par[k][1]));
		    break;
		case BETAPRIOR:
		    M->setPrior(k,(Prior*)new BetaPrior(prior_par[k][0],prior_par[k][1]));
		    break;
		case GAMMAPRIOR:
		    M->setPrior(k,(Prior*)new GammaPrior(prior_par[k][0]));
		    break;
		case GREATERPRIOR:
		    M->setPrior(k,(Prior*)new GreaterPrior(prior_par[k][0]));
		    break;
		case LESSPRIOR:
		    M->setPrior(k,(Prior*)new LessPrior(prior_par[k][0]));
		    break;
		default:
		    return NULL;
	    }
	}

	std::vector<double> theta;
	theta = M->MAP(.01);

	mapestimate = (PyArrayObject*) PyArray_FromDims(1,&Nparams,PyArray_DOUBLE);
	for (k=0; k<Nparams; k++)
		*((double*)(mapestimate->data+mapestimate->strides[0]*k)) = theta[k];

	delete M;

	return PyArray_Return(mapestimate);
}

/*************************************************************************************
 * loglikelihood function for the model                                              *
 *************************************************************************************/

static PyObject* loglikelihood ( PyObject* self, PyObject* args, PyObject* kwargs ) {
	int Nlevels;
	PyObject *pyresponses, *pyparameters;
	PyObject *pyrcount;
	char *modelname = "equalvariance";
	Model *M;
	int k,m; // Some helper variables
	bool mixturesdt(false);

	// Parse command line
	static char *kwlist[] = {"nlevels","responses","theta","modelname",NULL};
	if (!PyArg_ParseTupleAndKeywords( args, kwargs, "iOO|s", kwlist,
				&Nlevels, &pyresponses, &pyparameters, &modelname ))
		return NULL;


	// extract responses
	std::vector<int> responses(2*Nlevels);
	if (PySequence_Check(pyresponses)) {
		// pyresponses provides sequence protocol

		m = PySequence_Size(pyresponses);

		if (m != 2*Nlevels) { // Sequence has the wrong length
			PyErr_Format(PyExc_ValueError,
					"Number of response counts should be twice the number of"
					" confidence levels but it isn't.");
			return NULL;
		}

		// Go through sequence
		for (k=0; k<m; k++) {
			pyrcount = PySequence_GetItem(pyresponses,k);
			if (PyNumber_Check(pyrcount)) // All numbers are treated as valid response counts
				responses[k] = PyInt_AsLong(pyrcount);
			else {
				PyErr_Format(PyExc_ValueError,
						"Response counts should be integer.");
				return NULL;
			}
			Py_DECREF(pyrcount);
		}

	} else { // Response counts are no sequence at all

		PyErr_Format(PyExc_ValueError,
				"Response counts should be a sequence.");
		return NULL;
	}

	// Select the correct model equation
	if (!strcmp(modelname,"equalvariance"))
		M = new Model(Nlevels,Nlevels,responses,&equalvariance);
	else if (!strcmp(modelname,"gumbelsdt"))
		M = new Model(Nlevels,Nlevels,responses,&gumbelsdt);
	else if (!strcmp(modelname,"mixturegauss")) {
		M = new Model(Nlevels+1,Nlevels,responses,&mixturegauss);
		mixturesdt = true;
	} else if (!strcmp(modelname,"mixturegumbel")) {
		M = new Model(Nlevels+1,Nlevels,responses,&mixturegumbel);
		mixturesdt = true;
	} else {
		PyErr_Format(PyExc_ValueError,
				"Invalid sdt model selected");
		return NULL;
	}

	// Set priors
	double dl(1./(Nlevels-1));
	for (k=0; k<Nlevels-1; k++)
		M->setPrior(k,(Prior*)new GaussianPrior(k*dl,1e20));
	M->setPrior(Nlevels-1,(Prior*)new GaussianPrior(1,1e20));
	if (mixturesdt) M->setPrior(Nlevels,(Prior*)new BetaPrior(1.,1.));

	int Nparams = M->getnpar();
	std::vector<double> theta(Nparams);
	double loglike;
	if (PySequence_Check(pyparameters))
		{
			m = PySequence_Size(pyparameters);
			if ( m!= M->getnpar()) {
				PyErr_Format(PyExc_ValueError,
						"Expected %d parameters as starting values but got %d",M->getnpar(),m);
				return NULL;
			}
			for (k=0; k<M->getnpar(); k++) {
				pyrcount = PySequence_GetItem(pyparameters,k);
				if (PyNumber_Check(pyrcount))
					theta[k] = PyFloat_AsDouble(pyrcount);
				else {
					PyErr_Format(PyExc_ValueError,
							"starting values should be numbers.");
					return NULL;
				}
				Py_DECREF(pyrcount);
			}
			// Starting values are complete ~> start sampling
			M->logposterior(theta,&loglike);
		} else { // starting values are no sequence at all
			PyErr_Format(PyExc_ValueError,
					"starting values should be a sequence or None.");
			return NULL;
		}

	delete M;

	return PyFloat_FromDouble(loglike);
}

/*************************************************************************************
 * infrastructure for the module                                                     *
 *************************************************************************************/

static PyMethodDef bayessdt_methods[] = {
	{"mcmc_roc",(PyCFunction)mcmc_roc, METH_VARARGS | METH_KEYWORDS, mcmc_roc_doc},
	{"map_roc",(PyCFunction)map_roc,   METH_VARARGS | METH_KEYWORDS, map_roc_doc},
	{"loglikelihood",(PyCFunction)loglikelihood,   METH_VARARGS | METH_KEYWORDS, loglikelihood_doc},
	{NULL,NULL}
};

extern "C" {
void init_bayessdt() {
	(void)Py_InitModule("_bayessdt",bayessdt_methods);
	import_array();
}
}
