/*
 * bayessdt -- bayesian inference for rating based ROC curves
 * Copyright (C) 2009  Ingo Fründ
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */


#ifndef SDTMODELS_H
#define SDTMODELS_H

#include <cmath>
#include <vector>

#include "prior.h"
#include "constants.h"

double safe_log(double);

typedef double(*modellikelihood)(int,const std::vector<double>&,std::vector<int>&);

/** \defgroup sdtmodels Posterior distribution models for SDT models
 * \{
 */

/**
 * \brief Standard SDT model with equal variance for signal and noise
 *
 * Standard model of Signal Detection Theory. This model assumes gaussian distributions
 * for signal and noise and postulates equal variances for these two distributions.
 * Sensitivity is quantified by d'. For rating responses, the probability of
 * responses is
 * \f[
   P(r>k | s) = \int_{c_k}^\infty \mathcal{N}(x | -d,1)\,dx = 1-\Phi(c_k-d),
 * \f]
 * for signal present trials and
 * \f[
   P(r>k | n) = \int_{c_k}^\infty \mathcal{N}(x | 0,1)\,dx = 1-\Phi(c_k),
 * \f]
 * for noise only trials.
 */
double equalvariance(int,const std::vector<double>&,std::vector<int>&);

/**
 * \brief Mixture SDT model based on normal distributions
 *
 * DeCarlo (2001) proposed an extension of Signal Detection Theory: In this model, an
 * additional attention factor is included. On some trials, the observer lapses and does not
 * see the stimulus. Thus, for rating responses, the probabilities are
 * \f[
   P(r>k | s) = \lambda \int_{c_k}^\infty \mathcal{N}(x | -d,1)\,dx +
               (1-\lambda)\int_{c_k}^\infty \mathcal{N}(x | 0,1)\,dx =
			   \lambda (1-\Phi(c_k-d)) + (1-\lambda) (1-\Phi(c_k))
 * \f]
 * for signal present trials and
 * \f[
   P(r>k | n ) = \int_{c_k}^\infty \mathcal{N}(x | 0,1)\,dx = 1-\Phi(c_k),
 * \f]
 * for noise only trials.
 */
double mixturegauss(int,const std::vector<double>&,std::vector<int>&);

/**
 * \brief Standard SDT model based on gumbel distribution
 *
 * Similar to the equal variance signal detection model. In contrast to the classical model,
 * this model assumes a Gumbel distribution for signal and noise. The gumbel distribution is
 * skewed and arises as the distribution of the maximum of a large number of independent
 * channels. Sensitivity is quantified by a parameter d that describes the shift between the
 * signal and noise distribution.
 * For rating responses the response probabilites are given by
 * \f[
 * P(r>k | s) = \int_{c_k}^\infty Gumbel(x-d)\,dx = \exp(-\exp(x-d)),
 * \f]
 * for signal present trials and
 * \f[
 * P(r>k|n) = \int_{c_K}^\infty Gumbel(x)\,dx = \exp(-\exp(x)),
 * \f]
 * for noise only trials.
 */
double gumbelsdt(int,const std::vector<double>&,std::vector<int>&);

/**
 * \brief Mixture model for gumbel based SDT
 *
 * Similar to the mixture model extension for the normal distribution, this model implements
 * a mixture model extension based on the Gumbel distribution.
 */
double mixturegumbel(int,const std::vector<double>&,std::vector<int>&);

// TODO: Poisson distribution?

/** \} */
/**
 * \brief cumulative density function of the gaussian distributions
 */
double normcdf(double x);

/**
 * \brief cumulative density function of the gumbel distribution
 */
double gumbelcdf(double x);

/**
 * Likelihood for equal spread SDT models
 */
double simplelikelihood(int,const std::vector<double>&,std::vector<int>&,double (*)(double));

/**
 * Likelihood for mixture SDT models
 */
double mixturelikelihood(int,const std::vector<double>&,std::vector<int>&,double (*)(double));

/**
 * \brief generic model class for all signal detection models
 */
class Model
{
private:
	// double (*modeleq)(int,std::vector<double>&,std::vector<Prior*>&,std::vector<int>&);
	modellikelihood modeleq;
	std::vector<Prior*> priors;
	std::vector<int>    responses;
	std::vector<double> parameters;
	int nlevels;
	int nparameters;
public:
	/**
	 * Create a SDT model.
	 */
	Model(
			int, /**< Number of parameters */
			int, /**< Number of response alternatives */
			const char *, /**< Name of datafile */
			// double (*)(int,std::vector<double>&,std::vector<Prior*>&,std::vector<int>&) /**< Model likelihood */
            // posterior
			modellikelihood
			);
	Model(
			int, /**< Number of parameters */
			int, /**< Number of response alternatives */
			std::vector<int>&, /**<< response counts */
			// posterior /**< Model posterior */
			modellikelihood /** << Likelihood function of the model */
			);
	~Model();
	/**
	 * Set a prior
	 */
	void setPrior(
			int, /**< Index of the parameter to set the prior for */
			Prior* /**< Prior object that specifies the prior information */
			);
	/**
	 * Calculate the log of the unnormalized posterior distribution
	 *
	 * Note that calculation of the posterior combines two things:
	 * - the likelihood function of the model
	 * - the prior distribution for the parameters.
	 * Thus, calling this function before all priors are set, can result in segmentation faults.
	 */
	double logposterior(const std::vector<double>&);
	double logposterior(const std::vector<double>&,double*);

	/**
	 * Determine the number of parameters for the current model
	 */
	int getnpar(void);

	/**
	 * Determine a good starting value for MCMC integration. Typically this will be the
	 * MAP estimate (or something close to that).
	 * The MAP estimate is found using Simplex optimization of the negative log posterior.
	 */
	std::vector<double> getstart(void);

	/**
	 * Determine the MAP estimate using simplex optimization of the negative log posterior.
	 * Maximum stepwidth should be given for estimation.
	 */
	std::vector<double> MAP(double);
};

#endif
