/*
 * bayessdt -- bayesian inference for rating based ROC curves
 * Copyright (C) 2009  Ingo Fründ
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */


#ifndef RNG_H
#define RNG_H

#include <cmath>
#include <cstdlib>

/**
 * \brief Base class for random number generators
 */
class rng {
	public:
		rng (void ) {};
		virtual double sample(void) {};
};

/**
 * \brief Uniform distribution random number generator
 */
class rng_uniform : public rng {
	private:
		double low;
		double high;
	public:
		rng_uniform(double l=-1, double h=1);
		double sample (void);

};

/**
 * \brief Gaussian random numbers using the Box-Muller transform
 */
class rng_gaussian : public rng{
	private:
		double x1;
		double x2;
		double y;
		double w;
		double sigma;
		int good;
	public:
		rng_gaussian (double std=1);
		double sample (void);
};
#endif
