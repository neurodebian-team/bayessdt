/*
 * bayessdt -- bayesian inference for rating based ROC curves
 * Copyright (C) 2009  Ingo Fründ
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */


#include "sdtmodels.h"
#include <cstdio>
#include <iostream>

double safe_log(double x) {
    return x<=0 ? 1e20*(-1+x) : log(x);
}

double equalvariance(
	int nlevels,
	const std::vector<double>& parameters,
	std::vector<int>&    responses)
{
	double l(1);

	/**********************************************
	 * Determine the likelihood
	 */

	l = simplelikelihood(nlevels,parameters,responses,&normcdf);

	if (l==l)
	    return l;     // l is a well defined number
	else
	    return -1e20; // l is nan (e.g. nonincreasing criteria)
}

double mixturegauss(
	int nlevels,
	const std::vector<double>& parameters,
	std::vector<int>&    responses)
{
	double l(1);

	/**********************************************
	 * Determine the likelihood
	 */

	l = mixturelikelihood(nlevels,parameters,responses,&normcdf);

	if (l==l)
	    return l;     // l is a well defined number
	else
	    return -1e20; // l is nan (e.g. nonincreasing criteria)
}

double gumbelsdt(
		int nlevels,
		const std::vector<double>& parameters,
		std::vector<int>&    responses)
{
	double l;

	/*****************************************
	 * Determine the likelihood
	 */
	l = simplelikelihood(nlevels,parameters,responses,&gumbelcdf);

	if (l==l)
		return l;       // l is a well defined number
	else
		return -1e20;   // l is nan (e.g. due to nonincreasing criteria)
}

double mixturegumbel(
		int nlevels,
		const std::vector<double>& parameters,
		std::vector<int>&    responses)
{
	double l;

	/***********************************************
	 * Determine likelihood
	 */
	l = mixturelikelihood(nlevels,parameters,responses,&gumbelcdf);

	if (l==l)
	    return l;     // l is a well defined number
	else
	    return -1e20; // l is nan (e.g. nonincreasing criteria)
}

Model::Model(
	int npar,
	int nlev,
	const char * filename,
	//double (*eq)(int,std::vector<double>&,std::vector<Prior*>&,std::vector<int>&)
	modellikelihood eq
	)
: nparameters(npar), nlevels(nlev), modeleq(eq), priors(npar), responses(2*nlev), parameters(npar)
{
	int k;
	FILE *f = fopen(filename,"r");

	for (k=0; k<2*nlevels; k++)
	{
		fscanf(f,"%d ",&(responses[k]));
		std::cout << responses[k] << std::endl;
	}

	fclose(f);
}

Model::Model(
		int npar,
		int nlev,
		std::vector<int>& respcounts,
		modellikelihood eq
		)
: nparameters(npar), nlevels(nlev), modeleq(eq), priors(npar), responses(2*nlev), parameters(npar)
{
	int k;

	if (respcounts.size() != 2*nlev)
		throw -1;

	for (k=0; k<2*nlev; k++)
		responses[k] = respcounts[k];
}

Model::~Model(void)
{
	int k;
	for (k=0; k<nparameters; k++)
		delete priors[k];
}

void Model::setPrior(int position, Prior* thisprior)
{
	priors[position] = thisprior;
}

double Model::logposterior(const std::vector<double>& params)
{
	double dummy,p;
	int k;
	return logposterior(params,&dummy);
}

double Model::logposterior(const std::vector<double>& params,double *likelihood)
{
	double p;
	int k;
	// evaluate likelihood on old parameterization
	for (k=0; k<nparameters; k++)
	    parameters[k] = params[k];
	for (k=1; k<nlevels-1; k++)
		parameters[k] = params[k] + parameters[k-1];
	p = (*modeleq)(nlevels,parameters,responses);
	*likelihood = p;

	// Evaluate priors on new parameterization
	for (k=0; k<nparameters; k++)
	    p += safe_log((priors[k])->pdf(params[k]));
	return p;
}


int Model::getnpar(void)
{
	return nparameters;
}

std::vector<double> Model::getstart(void)
{
	return MAP(0.01);
}

std::vector<double> Model::MAP(double maxstep)
{
    // This should return the MAP-Estimate at best
    // here, we do it differently:
    // we return just zeros
	std::vector<double> start(nparameters);

	int ncrit,k,l;
	double dl;
	if (modeleq==equalvariance)
	{
		std::cerr << "Equal variance SDT model\n";
		ncrit = nparameters-1;
		start[nparameters-1] = 1.;  // d parameter
	} else if (modeleq==mixturegauss)
	{
		std::cerr << "Mixture gaussian SDT model\n";
		ncrit = nparameters-2;
		start[nparameters-2] = 1.;  // d parameter
		start[nparameters-1] = .5;  // lm parameter
	} else if (modeleq==mixturegumbel)
	{
		std::cerr << "Mixture Gumbel SDT model\n";
		ncrit = nparameters-2;
		start[nparameters-2] = 1.;  // d parameter
		start[nparameters-1] = .5;  // lm parameter
	} else if (modeleq==gumbelsdt)
	{
		std::cerr << "Gumbel SDT model\n";
		ncrit = nparameters-1;
		start[nparameters-1] = 1;   // d parameter
	}
	dl = 1./(ncrit-1);

	start[0] = 0;
	for (k=1; k<ncrit; k++)
	    start[k] = dl;


	/* Now perform simplex optimization to find the MAP estimate */
	std::vector< std::vector<double> > simplex (nparameters+1,start);
	std::vector<double> fx(nparameters+1);
	std::vector<double> x(nparameters);
	std::vector<double> xx(nparameters);
	for (k=1; k<nparameters+1; k++) simplex[k][k-1] += .9*dl;
	double ffx;
	int maxind(0), minind(0);
	double stepsize;
	int iter(0),maxiter(400);

	/*// Print the simplex
	for (k=0; k<nparameters+1; k++)
	{
		std::cerr << "\n";
		for (l=0; l<nparameters; l++)
			std::cerr << simplex[k][l] << "\t";
	}
	*/

	while (1) {
		maxind = minind = 0;
		for (k=0; k<nparameters+1; k++)
		{
			fx[k] = -logposterior(simplex[k]);
			if (fx[k]<fx[minind]) minind = k;
			if (fx[k]>fx[maxind]) maxind = k;
		}

		// Calculate the average of the non maximal nodes
		for (k=0; k<nparameters; k++) x[k] = 0;
		for (k=0; k<nparameters+1; k++)
		{
			if (k!=maxind)
				for (l=0; l<nparameters; l++) x[l] += simplex[k][l];
		}
		for (k=0; k<nparameters; k++) x[k] /= nparameters;

		// Reflected worst point
		for (k=0; k<nparameters; k++) xx[k] = x[k] - (simplex[maxind][k]-x[k]);

		// Now check what to do
		ffx = -logposterior(xx);
		if (ffx<fx[minind]) {
			// Better than previous best point ~> Expand
			for (k=0; k<nparameters; k++) simplex[maxind][k] = x[k] - 2*(simplex[maxind][k]-x[k]);
		} else if (ffx>fx[maxind]) {
			// The reflected worst point is even worse ~> Shrink
			for (k=0; k<nparameters+1; k++)
			{
				for (l=0; l<nparameters; l++)
					simplex[k][l] = simplex[minind][l] + 0.5 * (simplex[k][l]-simplex[minind][l]);
			}
		} else {
			// reflected point is somewhere between
			for (k=0; k<nparameters; k++) simplex[maxind][k] = xx[k];
		}

		// Calculate stopping criterion
		stepsize = 0;
		for (k=0; k<nparameters; k++)
			stepsize += (x[k]-xx[k])*(x[k]-xx[k]);
		// std::cerr << "\n" << stepsize;
		if (stepsize<maxstep) break;

		// Check maximum number of iterations
		if (iter++>maxiter) break;
	}

	std::cerr << "Starting values:\n";
	for (k=0; k<nparameters; k++) {
		start[k] = simplex[minind][k];
		std::cerr << start[k] << "\t";
	}
	std::cerr << "\n";

	return start;
}

double normcdf (double x)
{
	return .5*(1.+ erf(x/SQRT2));
}

double gumbelcdf(double x)
{
	return 1-exp(-exp(x));
}

double simplelikelihood(
		int nlevels,
		const std::vector<double>& parameters,
		std::vector<int>&    responses,
		double (*cdf)(double)
		)
{
	double l(0);
	double d(parameters[nlevels-1]);
	int k;

	// Probability that the stimulus' strength is below the lowest criterion
	l  = responses[nlevels+0] * log((*cdf)(parameters[0]))
	    + responses[0] * log((*cdf)(parameters[0] - d));
	// Probability that the stimulus' strength is above the highest criterion
	l += responses[2*nlevels-1] * log(1-(*cdf)(parameters[nlevels-2]))
	    + responses[nlevels-1] * log(1-(*cdf)(parameters[nlevels-2] - d));

	// loop over the remaining criteria
	for (k=1; k<nlevels-1; k++) {
	    l +=  responses[nlevels+k] * log((*cdf)(parameters[k]) - (*cdf)(parameters[k-1]))
		+ responses[k] * log((*cdf)(parameters[k] - d) - (*cdf)(parameters[k-1] -d));
	}

	return l;
}

double mixturelikelihood(
		int nlevels,
		const std::vector<double>& parameters,
		std::vector<int>&    responses,
		double (*cdf)(double)
		)
{
	double l;
	double d(parameters[nlevels-1]);
	double lm(parameters[nlevels]);
	int k;
	double p0;

	// Probability that the stimulus' strength is below the lowest criterion
	p0 = (*cdf)(parameters[0]);
	l  = responses[nlevels+0] * log(p0)
	    + responses[0] * log(lm*(*cdf)(parameters[0] - d)+(1-lm)*p0);
	// Probability that the stimulus' strength is above the highest criterion
	p0 = 1-(*cdf)(parameters[nlevels-2]);
	l += responses[2*nlevels-1] * log(p0)
	    + responses[nlevels-1] * log(lm*(1-(*cdf)(parameters[nlevels-2] - d)) + (1-lm)*p0);

	// loop over the remaining criteria
	for (k=1; k<nlevels-1; k++) {
	    p0 = (*cdf)(parameters[k]) - (*cdf)(parameters[k-1]);
	    l +=  responses[nlevels+k] * log(p0)
		+ responses[k] * log(lm*((*cdf)(parameters[k] - d) - (*cdf)(parameters[k-1] -d))
			+ (1-lm)*p0);
	}

	return l;
}
