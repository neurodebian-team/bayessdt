/*
 * bayessdt -- bayesian inference for rating based ROC curves
 * Copyright (C) 2009  Ingo Fründ
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */


#include "rng.h"

rng_uniform::rng_uniform (double l, double h) :
	low(l), high(h)
{
	if (high < low)
		low = h , high = l;
}

double rng_uniform::sample (void)
{
	return (high-low) * drand48() - low;
}

rng_gaussian::rng_gaussian (double std) :
	good(0), sigma(std)
{}

double rng_gaussian::sample (void)
{
	if (good) {
		good = 0;
		return y;
	} else {
		do {
			x1 = 2*drand48() - 1;
			x2 = 2*drand48() -1;
			w = x1*x1 + x2*x2;
		} while (w>=1.0);

		w = sqrt( (-2.*log(w))/w);
		y = x2*w;
		good = 1;
		return x1*w;
	}
}
