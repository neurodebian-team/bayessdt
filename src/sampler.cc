/*
 * bayessdt -- bayesian inference for rating based ROC curves
 * Copyright (C) 2009  Ingo Fründ
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */


#include "sampler.h"
#include <iostream>

MetropolisHastingsSampler::MetropolisHastingsSampler(
		unsigned int nsamples,
		unsigned int nparams,
		Model *model)
:
	nparameters(nparams),
	stepwidths(nparams,1.),
	samples(nparams,std::vector<double>(nsamples)),
	nsamples(nsamples),
	lsamples(nsamples,0),
	accept(nsamples)
{
	if (model->getnpar()!=nparameters)
		throw -2;
	M = model;
	rgen = new rng_gaussian();
}

double MetropolisHastingsSampler::sample(void) {
	// Basically the same as with a defined starting point...
	return sample(M->getstart());
}

double MetropolisHastingsSampler::sample(const std::vector<double>& start) {
	// Here comes the tricky part...
	std::vector<double> current(start);
	std::vector<double> candidate(nparameters);
	int l;
	int m(0);
	double alpha;
	// double lcurrent (((MixtureSDTgaussian*)M)->logposterior(current));
	double curlikelihood,canlikelihood;
	double lcurrent (M->logposterior(current,&curlikelihood));
	double lcandidate(0);
	double lratio;
	double acceptancerate(1);

	while (m<nsamples)
	{
		accept[m] = 0;
		for (l=0; l<nparameters; l++)
			candidate[l] = current[l] + rgen->sample()*stepwidths[l];
		// lcandidate = ((MixtureSDTgaussian*)M)->logposterior(candidate);
		lcandidate = M->logposterior(candidate,&canlikelihood);
		lratio = exp(lcandidate-lcurrent);
		alpha = (lratio < 1 ? lratio : 1);
		if (drand48() < alpha) {
			current = candidate;
			lcurrent = lcandidate;
			curlikelihood = canlikelihood;
			acceptancerate++;
			accept[m] = 1;
		}
		for (l=0; l<nparameters; l++) {
			samples[l][m] = current[l];
			lsamples[m] = curlikelihood;
		}
		m++;
	}
	return acceptancerate/nsamples;
}

void MetropolisHastingsSampler::printsamples(std::ofstream& outfile) {
	int k,l;
	outfile << "# ";
	for (k=0; k<nparameters; k++)
		outfile << "P" << k << " ";
	outfile << " l accept\n";
	for (k=0; k<nsamples; k++) {
		for (l=0; l<nparameters; l++)
			outfile << " " << samples[l][k];
		outfile << " " << lsamples[k] << " " << accept[k] << "\n";
	}
}

void MetropolisHastingsSampler::setstep(int k, double w) {
	stepwidths[k] = w;
}

void MetropolisHastingsSampler::getsamplesarray(double *th,double *ll,int *a) {
	int k,l;

	for (k=0; k<nsamples; k++) {
		for (l=0; l<nparameters; l++)
			th[k*nparameters+l] = samples[l][k];
		ll[k] = lsamples[k];
		a[k] = accept[k];
	}
}
