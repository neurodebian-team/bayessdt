/*
 * bayessdt -- bayesian inference for rating based ROC curves
 * Copyright (C) 2009  Ingo Fründ
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */


#include "prior.h"
#include <iostream>

GaussianPrior::GaussianPrior(double m,double s) :
	mu(m), sg(s), var(s*s)
{}

double GaussianPrior::pdf(double theta)
{
	return exp(-0.5*pow(theta-mu,2)/var)/(sg*SQRT2PI);
}

double GaussianPrior::getstart(void) {
	return mu;
}

BetaPrior::BetaPrior(double al, double bt) :
	alpha(al), beta(bt)
{}

double BetaPrior::pdf(const double x)
{
	if (x>1) return 0;
	else if (x<0) return 0;
	else return pow(x,alpha-1)*pow(1-x,beta-1)/betaf(alpha,beta);
}

double BetaPrior::getstart(void) {
	return alpha/(alpha+beta);
}

GammaPrior::GammaPrior(double df) :
    nu(df) {
	normalizer = pow(2,0.5*nu) * exp(gammaln(0.5*nu));
}

double GammaPrior::pdf(const double x) {
	return x>0 ? pow(x,nu-2)*exp(-x/2) / normalizer : 0;
}

double GammaPrior::getstart(void) {
	return nu;
}

GreaterPrior::GreaterPrior(double xxi) :
    xi(xxi)
{}

double GreaterPrior::pdf(const double x)
{
    return x>=xi;
}

double GreaterPrior::getstart(void) {
    return xi+10;
}

LessPrior::LessPrior(double xxi) :
    xi(xxi)
{}

double LessPrior::pdf(const double x)
{
    return x<=xi;
}

double LessPrior::getstart(void) {
    return xi-10;
}

double gammaln(double xx) {
	// More or less copied from Numerical Recipes
	double x,y,tmp,ser;
	static double cof[6]={
		76.18009172947146,
		-86.50532032941677,
		24.01409824083091,
		-1.231739572450155,
		0.1208650973866179e-2,
		-0.5395239384953e-5 };
	int j;

	y=x=xx;
	tmp=x+5.5;
	tmp -= (x+.5)*log(tmp);
	ser=1.000000000190015;
	for (j=0; j<=5; j++) ser += cof[j]/++y;
	return -tmp+log(2.5066282746310005*ser/x);
}

double betaf(double z, double w) {
	return exp(gammaln(z)+gammaln(w)-gammaln(z+w));
}
