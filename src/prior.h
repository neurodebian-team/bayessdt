/*
 * bayessdt -- bayesian inference for rating based ROC curves
 * Copyright (C) 2009  Ingo Fründ
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
 * USA
 */


#ifndef PRIOR_H
#define PRIOR_H

#include <cmath>
#include <vector>
#include <iostream>

#include "constants.h"

/**
 * \brief Generic Prior class
 */
class Prior {
	public:
		Prior (void) {};                               /**< This basically does nothing */
		virtual double pdf (const double) {return 0;}  /**< Returns the pdf */
		virtual double getstart(void) {return 0;}      /**< Find a starting value based on prior */
};

/**
 * \brief Gaussian Prior distribution
 *
 * The Density of the gaussian is
 * \f[
 * f(x|\mu,\sigma^2) = \frac{1}{\sqrt(2\pi)\sigma} \exp\big( -\frac{(x-\mu)^2}{2\sigma^2}\big).
 * \f]
 */
class GaussianPrior : public Prior {
	private:
		double mu;
		double sg;
		double var;
	public:
		GaussianPrior (double,double);  /**< Initialize Gaussian prior giving mean and variance */
		double pdf(const double);       /**< Determine prior probability for a parameter value */
		double getstart(void);          /**< get a starting value based on prior information */
};

/**
 * \brief Beta Prior Distribution
 *
 * The density of the Beta distribtion is
 * \f[
 * f(x|\alpha,\beta) = \frac{\Gamma(\alpha+\beta)}{\Gamma(\alpha)\Gamma(\beta)} x^{\alpha-1}(1-x)^{\beta-1}
 * \f]
 * The support of this density is limited to \f$x\in(0,1)\f$.
 * The Beta distribution is a suitable prior for variables to resemble the success probability of
 * the binomial distribution. For that case, \f$\alpha\f$ and \f$\beta\f$ can be interpreted in
 * terms of the number of prior successes and failures. The Beta(\f$\alpha,\beta\f$) distribution
 * is the probability distribution of the binomial parameter \f$p\f$ after \f$\alpha-1\f$ successes
 * and \f$\beta-1\f$ failures. Thus in particular Beta(1,1) is a flat prior.
 */
class BetaPrior : public Prior {
	private:
		double alpha;
		double beta;
	public:
		BetaPrior(double,double);   /**< Initialize Beta Prior giving parameters alpha and beta */
		double pdf(const double);   /**< evaluate the Beta for a given value */
		double getstart(void);      /**< get a starting value based on prior information */
};

/**
 * \brief Gamma Prior Distribution
 *
 * The density of the Gamma distribution is
 * \f[
 * f(x|\nu) = \frac{x^{(\nu-2)/2} \exp(-x/2)}{2^{\nu/2} \Gamma(\nu/2)}
 * \f]
 * The support of this density is limited to \f$x>=0\f$.
 * The Gamma distribution is a suitable prior for variables that resemble the squuare of a normal
 * distribution. For instance like a distance.
 */
class GammaPrior : public Prior {
	private:
		double nu;
		double normalizer;
	public:
		GammaPrior(double);        /**< Initialize Gamma prior giving the parameter nu */
		double pdf(const double);  /**< evaluate the Gamma density for a a given value */
		double getstart(void);     /**< get a starting value based on prior information */
};

/**
 * \brief Improper prior for numbers greater than a specified number
 *
 * This prior is not normalized to 1. It gives a constant value of 1 for all values greater than a specified value xi
 * and zero otherwise.
 */
class GreaterPrior : public Prior {
	private:
		double xi;
	public:
		GreaterPrior(double);     /**< Initialize GreaterPrior with parameter xi */
		double pdf(const double); /**< evaluate the GreaterPrior for a given value */
		double getstart(void);    /**< get a starting value based onprior information */
};

/**
 * \brief Improper prior for numbers less than a specified number
 *
 * This prior is not normalized to 1. It gives a constant value of 1 for  all values less than a specified value xi
 * and zero otherwise.
 */
class LessPrior : public Prior {
	private:
		double xi;
	public:
		LessPrior(double);        /**< Initialize LessPrior with parameter xi */
		double pdf(const double); /**< evaluate the LessPrior for a given value */
		double getstart(void);    /**< get a startiing value based on prior information */
};

/** logarithm of the gamma distribution -- this is used for the beta distribution */
double gammaln(double);
/** beta function -- this is used for the beta distribution */
double betaf(double,double);

#endif
