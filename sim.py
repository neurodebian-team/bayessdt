#!/usr/bin/env python

from numpy import *

x = random.randn(1000)
x[500:] += 1.

r = zeros(1000)
r[x>0] += 1
r[x>.5] += 1
r[x>1] += 1

s = [sum(r[500:]==k) for k in xrange(4)]
n = [sum(r[:500]==k) for k in xrange(4)]

print s,n
