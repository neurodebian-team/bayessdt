#!/usr/bin/env python
# encoding: utf8
##
## bayessdt -- bayesian inference for rating based ROC curves
## Copyright (C) 2009  Ingo Fründ
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
## USA
##

from numpy import *
from scipy import stats
import _bayessdt
import pylab as p
import sys,re

__all__ = ["ROCmodel"]

__doc__ = """bayessdt -- estimation module

This module defines a single large ROCmodel object that supports
all the inference and data processing you might want to do on
a ROC model.

(c) 2009 by Ingo Fründ (ingo.fruend@googlemail.com)
"""

# Integer markers for different priors (as in the C-library)
GAUSSPRIOR = 0
BETAPRIOR  = 1
GAMMAPRIOR = 2
GREATERPRIOR = 3
LESSPRIOR = 4

class ROCmodel(object):
    def __init__(self,data,model="equalvariance",priors={}):
        """Initialize a model for a receiver operating characteristic

        :Parameters:
            data        response data from a rating experiment. Two formats
                        are valid.
                        1. a sequence of response counts in the following order:
                            <S0> <S1> ... <Sn> <N0> <N1> ... <Nn>,
                            where S/N indicates signal and noise (strictly speaking,
                            S indicates signal+noise and N indicates noise alone).
                        2. a rank-2 array of responses and stimuli. One column contains
                            the stimuli (there should be only two stimuli). The lower
                            stimulus considered the noise alone stimulus, the higher is
                            considered signal+noise. The second column should in this
                            case contain the confidence ratings of the observer.
                            Low values on the rating scale should correspond to
                            low confidence, high values on the rating scale should
                            correspond to high confidence
            model       a signal detection model for a receiver operating characteristic.
                        There are four valid choices.
                        1. 'equalvariance' the standard model assuming normal distributions
                            for signal+noise and for noise alone. The signal+noise distribution
                            is assumed to be a right-shifted version of the noise alone
                            distribution. Thus the variances for signal+noise and for
                            noise alone are the same. The parameters of this model
                            equal the number n of response alternatives. The parameter
                            vector contains first n-1 criteria c_i and then a single
                            discriminability parameter d.
                        2. 'gumbelsdt' is similar to 'equalvariance' but assumes
                            gumbel (extreme value) distributions for signal+noise and
                            for noise alone. Parameter order is like for the 'equalvariance'
                            model.
                        3. 'mixturegauss' this model assumes an additional attention
                            parameter a: on some trials the observer was unattentive. As a
                            result, a signal+noise stimulus only elecits a response that
                            corresponds to the noise alone distribution. Thus, the probability
                            for a confidence rating k is
                                P(k|S,a) = a*P(k|S,a==1)+(1-a)*P(k|N,a==1).
                            The probability distributions P(k|S,a==1) and P(k|N,a==1) correspond
                            to the 'equalvariance' model. The model has n+1 parameters, where
                            n is the number of response alternatives. Parameters 1 to n are
                            the same as for the 'equalvariance' model, n-1 criteria c_i and
                            a discriminability parameter d. The n+1-th parameter is the
                            attention parameter a as defined above.
                        4. 'mixturegumbel' this model is conceptually the same as the
                            'mixturegauss' model but assumes the underlying distributions to
                            be gumbel (extreme value) distributions.
                        Note that the two mixturemodels 3 and 4 can be tricky to estimate. In
                        particular, MAP-estimates for these models typically have not converged
                        indicating that the returned MAP-estimate is actually not a maximum of
                        the posterior distribution. In most cases MCMC estimation is more
                        stable. In some cases, convergence can be very bad. This can sometimes
                        be solved by simply estimating again.
            priors      a dictionary containing priors for the different parameters, e.g.
                        {'a': 'beta(1,1)'} puts a beta prior on the a parameter.
        """
        self.model = model
        data = array(data)
        if rank(data)==1:
            self.nlevels = len(data)/2
        elif rank(data)==2:
            if data.shape[0]==2:
                data = data.T
            n1,n2 = len(unique(data[:,0])),len(unique(data[:,1]))
            if n1==2:
                stim = data[:,0]
                resp = data[:,1]
            elif n2==2:
                stim = data[:,1]
                resp = data[:,0]
            stims = sort(unique(stim))
            resps = sort(unique(resp))
            data = []
            for s in stims[::-1]:
                for r in resps:
                    data.append(sum((resp==r)*(stim==s)))
            self.nlevels = len(resps)
        self.data = array(data)

        self.burnin = 0
        self.thin = 1

        # Set priors
        self._priors = []
        self._prior_specs = []
        ncrit = self.nlevels-1
        for k,dl in enumerate(mgrid[0:1:ncrit*1j]):
            if k == 0:
                self._prior_specs.append(priors.setdefault('c%d' % (k+1,),"gauss(0,1e20)"))
            else:
                self._prior_specs.append(priors.setdefault("c%d" % (k+1,),">0"))
            self._priors.append(prior2python(self._prior_specs[-1]))
        self._prior_specs.append(priors.setdefault("d","gamma(2)"))
        self._priors.append(prior2python(self._prior_specs[-1])) # d parameter
        if self.model in ["mixturegauss","mixturegumbel"]:
            self._prior_specs.append(priors.setdefault("a","beta(1,1)"))
            self._priors.append(prior2python(self._prior_specs[-1]))
        self.nprm = len(self._priors)

    ###################################
    # Estimation of parameters
    def estimate(self,strategy="mcmc"):
        """Estimate the paramters of the model

        :Parameters:
            strategy        Select the strategy for model estimation. In virtually all
                            cases, the correct choice is 'mcmc' which starts Markov Chain
                            Monte Carlo. An alternative strategy for some quick and dirty
                            scenarios might be 'MAP'. However, be aware of the fact that
                            MAP-estimation for mixed models will in virtually all cases
                            terminate without convergence (and without error messages!)
        """
        if strategy=="mcmc":
            self.MCMC()
        elif strategy=="MAP":
            self.MAP()
        else:
            raise ValueError,"valid estimation types are mcmc and MAP"

    def MCMC(self,Nsamples=200000,Nchains=3,stepsizes=None,resample=False):
        """Perform markov chain monte-carlo to draw samples from the posterior

        This functions provides finer control of the sampling process. If you don't want
        to use the default values, you should use this one for MCMC.

        :Parameters:
            Nsamples        number of samples to be drawn per chain.
            Nchains         number of chains to be run.
            stepsizes       either a sequence of stepsizes in the order
                            (cstep,dstep,[astep]), where astep is only used
                            for mixture models. Alternatively, a dictionary
                            can be used of the form
                            {"c":cstep,"d":dstep,"a":astep}
            resample        boolean variable that indicates whether a chain that did
                            not converge should be attempted to be replaced

        Note that this method does not have parameters to set the burnin or the thinning.
        These parameters can be set posthoc using the properties buinin and thin.
        """
        if Nchains<1:
            raise ValueError,"MCMC with less than 1 chain makes no sense"
        self._samples = []
        self.D       = []
	self._stepsizes = stepsizes
        priors = [prior2C(x) for x in self._prior_specs]
        for chain in xrange(Nchains):
            if chain==0:
                start = None;
            else:
                start = self.__getstart()
            if resample:
                while True:
                    sys.stderr.write("Sampling chain %d\n"%(chain,))
                    samples,l,a = _bayessdt.mcmc_roc(
                            self.nlevels,
                            self.data,
                            self.model,
                            start,
                            Nsamples,
                            stepsizes,
                            priors)
                    N = Nsamples/2
                    self.burnin = N
                    Y = array(samples[N:,:])-array(samples[N:,:]).mean(0)
                    Y /= Y.std(0)
                    n = N/10
                    m = array([mean(Y[k*n:(k+1)*n]) for k in xrange(10)])
                    v = array([var(Y[k*n:(k+1)*n]) for k in xrange(10)])
                    # The following conditions represent:
                    # 1. moving average should not be more than 2 sd from total average
                    # 2. moving variance should not be below the 20th percentile of the gamma(1) dist
                    # 3. moving variance should not be above the 80th percentile of the gamma(1) dist
                    # if abs(m).max()>2 or v.min()<0.22 or v.max()>1.61:
                    if abs(m).max()>1 or v.min()<0.52 or v.max()>1.31: # These are more strict => less likeli to have bad chains but more likeli to resample
                        sys.stderr.write(
                                "Chain %d did not converge -- resampling\n" \
                                        % (chain,))
                    else:
                        break
            else:
                samples,l,a = _bayessdt.mcmc_roc(
                    self.nlevels,
                    self.data,
                    self.model,
                    start,
                    Nsamples,
                    stepsizes,
                    priors)

            self._samples.append(array(samples))
            self.D.append(-2*array(l))
        self._map = self._samples[0][0,:]  # MCMC starts at the MAP estimate to reduce burnin
        self.Nchains = Nchains

    def MAP(self):
        """Use simplex optimization to obtain the maximum of the posterior

        Note that for mixture sdt models this does often not converge and thus the
        value stored as map estimate is actually only a 'nearly map' estimate.
        """
        priors = [prior2C(x) for x in self._prior_specs]
        self._map = _bayessdt.map_roc(self.nlevels,self.data,self.model,priors)
        return self._map

    def __getstart(self):
        """Get a starting value for a chain

        This is usually not called by the user
        """
        ncrit = self.nlevels-1
        c = [random.randn()*3]
        while len(c)<ncrit:
            c.append( random.randn()**2)
        c.append( random.randn()**2)
        if self.model in ["mixturegauss","mixturegumbel"]:
            c.append(random.uniform())
        return c

    ###################################
    # Fine tuning of the sampled chains
    def setburnin(self,value):
        """Set the burnin period"""
        self._burnin = value

    def setthin(self,value):
        """Set the thinning factor"""
        self._thin = value

    def delchain(self,index):
        """Delete a chain

        WARNING: This really deletes a chain! There is no way to recover the chain."""
        self.Nchains -= 1
        self._samples.pop(index)
        if self.Nchains == 0:
            raise IOError,"No chains left"

    def resample(self,chain):
	"""Resample a single chain

	Deletes a chanin and replaces it with a new chain. This is useful, if we observe that a single
	chain did not converge.

	:Parameters:
	    chain     index of the chain to be replaced
	"""
	if chain>=self.Nchains:
	    raise ValueError, "resampling of a non existent chain was requested"
	start = self.__getstart()
        priors = [prior2C(x) for x in self._prior_specs]
	samples,l,a = _bayessdt.mcmc_roc(
		self.nlevels,
		self.data,
		self.model,
		start,
		self._samples[chain].shape[0],
		self._stepsizes,
                priors)
	self._samples[chain] = array(samples)
	self.D[chain]        = -2*array(l)

    ####################################
    # Analyzing the chains
    def getacf(self,parameter,lags=range(20),chains=None):
        """autocorrelation function of the chains

        :Parameters:
            parameter   index of the model parameter
            lags        lags at which the acf should be calculated
            chains      which chains should be selected (None => all)
        """
        if chains is None:
            chains = range(len(self._samples))
        R = []
        Nsamples = self.chain(0).shape[0]
        for tau in lags:
            if tau==0:
                R.append(1)
                continue
            z = []
            for chain in chains:
                r = corrcoef(
                        self.chain(chain)[tau:,parameter],
                        self.chain(chain)[:Nsamples-tau,parameter])[0,1]
                z.append(0.5*(log(1+r)-log(1-r))) # Fisher's z
            z = mean(z)
            R.append((exp(2*z)-1)/(exp(2*z)+1))   # inverse Fisher's z
        return R

    def chain(self,index):
        """Get one chain from the sampled data

        :Parameters:
            index       index of the chain
        """
        return self._samples[index][self.burnin::self.thin,:]

    ####################################
    # Inference and convergence diagnostics
    def BayesFactor(self,parameter,value):
        """Compute the Bayes Factor to test the model against a simpler one with a fixed parameter

        :Parameters:
            parameter   index of the model parameter to test
            value       value at which the parameter is assumed to be fixed

        :Output:
            BayesFactor based on the savage dickey ratio
        """
        dist = stats.kde.gaussian_kde(self.samples[:,parameter])
        posterior = dist(value)
        prior = self._priors[parameter].pdf(value)
        return posterior/prior

    def GelmanRubin(self,parameter):
        """Gelman and Rubin diagnostic for chain convergence"""
        B = var(self.samples[:,parameter])
        W = 0
        for k in xrange(self.Nchains):
            W += var(self.chain(k)[:,parameter])
        W /= self.Nchains
        return B/W

    def getPI(self,parameter,p=.95):
        """Get Posterior intervalls for a parameter

        :Parameters:
            parameter   index of the model parameter to test
            p           determine the central <p> fraction of the posterior

        :Output:
            lowPI,highPI
        """
        p1 = 1-float(p)
        p1 *= 0.5
        p2 = 1-p1
        x = sort(self.samples[:,parameter])
        return x[p1*len(x)],x[p2*len(x)]

    ####################################
    # get the roc data

    def rocest(self,c=None):
        """Determine predictions of the model at criteria c

        :Parameters:
            c           criteria at which to evaluate (None=estimated criteria)

        :Output:
            predicted_false_alarms,predicted_hits
        """
        prm = self.mean_est
        if self.model=="equalvariance":
            cdf = stats.norm.cdf
            lm = 1     # Non mixture is equal to lm==1
            if c==None:
                c = prm[:-1]
            d = prm[-1]
        elif self.model=="gumbelsdt":
            cdf = stats.gumbel_l.cdf
            lm = 1     # Non mixture is equal to lm==1
            if c==None:
                c = prm[:-1]
            d = prm[-1]
        elif self.model=="mixturegauss":
            cdf = stats.norm.cdf
            lm = prm[-1]
            d = prm[-2]
            if c == None:
                c = prm[:-2]
        elif self.model=="mixturegumbel":
            cdf = stats.gumbel_l.cdf
            lm = prm[-1]
            d = prm[-2]
            if c == None:
                c = prm[:-2]

        # Calculate predicted values
        predicted_false_alarms = 1-cdf(c)
        predicted_hits = 1-lm*cdf(c-d)-(1-lm)*cdf(c)

        return predicted_false_alarms,predicted_hits

    def rocobs(self):
        """Determine the observed roc values

        :Output:
            false_alarms,hits
        """
        hits = cumsum(self.data[:self.nlevels]).astype("d")
        false_alarms = cumsum(self.data[self.nlevels:]).astype("d")
        return 1-false_alarms/false_alarms[-1],1-hits/hits[-1]

    ####################################
    # Plot summaries of the samples

    def display(self):
        self.summaryplot()
        for prm in xrange(self.nprm):
            self.prmconvplot(prm)

    def prmconvplot(self,prm):
        """convergence plot for a single parameter"""
        prmname = self.prmnames()[prm]
        convergence = p.figure()

        convergence.add_subplot(221)
        for k in xrange(self.Nchains):
            p.plot(self.chain(k)[:,prm])
        p.title("trace of parameter %s"%(prmname,))

        convergence.add_subplot(222)
        for k in xrange(self.Nchains):
            p.plot(self.getacf(prm,chains=[k]),'o-')
        p.title("auto correlation of %s"%(prmname,))

        mp = convergence.add_subplot(223)
        mv = convergence.add_subplot(224)
        for k in xrange(self.Nchains):
            x = self.chain(k)[:,prm]
            y = x-x.mean()
            y /= y.std()
            n = len(y)/10
            m = [mean(y[k*n:(k+1)*n]) for k in xrange(10)]
            v = [var(y[k*n:(k+1)*n]) for k in xrange(10)]
            mp.plot(m,'o-')
            mv.plot(v,'o-')
        mp.set_title("moving average of %s"%(prmname,))
        mp.set_ylim(-3,3)
        mp.plot([0,10],[-2,-2],'k:',[0,10],[2,2],'k:')
        mv.set_title("moving variance of %s"%(prmname,))
        mv.set_ylim(0,6)
        mv.plot([0,10],[3,3],'k:',[0,10],[1./3,1./3],'k:')

    def summaryplot(self):
        """Show a simple diagnostic plot of the samples"""
        summary = p.figure()
        # # Here should be convergence diagnostics
        # prmfigs = []
        # for prm in xrange(self.nprm):
        #     prmfigs.append(p.figure())

        summary.add_subplot(221)
        cols = []
        for prm in xrange(self.nprm):
            cols.append(p.plot([prm,prm,prm],p.prctile(self.samples[:,prm],(2.5,50,97.5)),'+-'))
        p.setp(p.gca(),xticks=range(0,self.nprm),xticklabels=self.prmnames())
        p.xlim(-1,self.nprm)
        p.title("Parameter estimates and 95% posterior intervals")

        summary.add_subplot(222)
        for prm in xrange(self.nprm):
            h,b = histogram(self.samples[:,prm],50,normed=True)
            # p.step(b[:-1],h)
            p.step(b,h)
        p.title("Histograms")

        summary.add_subplot(223)
        p.plot([-1,self.nprm],[1,1],'k:')
        for prm in xrange(self.nprm):
            p.plot([prm],[self.GelmanRubin(prm)],'o')
        p.setp(p.gca(),xticks=range(0,self.nprm),xticklabels=self.prmnames())
        p.xlim(-1,self.nprm)
        p.ylim(0,3)
        p.title("Gelman-Rubin statistics")

        summary.legend(cols,self.prmnames())

        summary.text(.5,.5,self.__str__(),verticalalignment="top")

    ####################################
    # Helper functions for representation

    def prmnames(self):
        """return a list of parameternames"""
        if self.model=="equalvariance":
            names = ["c%d"%(x+1,) for x in xrange(self.nprm-1)]
            names.append("d'")
        elif self.model=="gumbelsdt":
            names = ["c%d"%(x+1,) for x in xrange(self.nprm-1)]
            names.append("d")
        elif self.model in ["mixturegauss","mixturegumbel"]:
            names = ["c%d"%(x+1,) for x in xrange(self.nprm-2)]
            names.append("d")
            names.append("a")
        return names

    def __repr__(self):
        """Short version"""
        return "%s ROCmodel" % (self.model,)

    def __str__(self):
        """long version"""
        s = "%s ROCmodel\n" %(self.model,)
        s +=     "  Prm\t2.5%\t50%\t97.5%\tmean\tR^\n"
        m = self.mean_est
        for k,n in enumerate(self.prmnames()):
            prc = p.prctile(self.samples[:,k],(2.5,50,97.5))
            s += "  %s\t%4.3f\t%4.3f\t%4.3f\t%4.3f\t%.3f\n"% \
                    (n,prc[0],prc[1],prc[2],m[k],self.GelmanRubin(k))
        s += "%d chains, %d samples\n"%(self.Nchains,self._samples[0].shape[0])
        s += "burnin = %d, thinning = %d\n"%(self.burnin,self.thin)
        s += "total effective samples = %d\n"%(self.samples.shape[0],)
        s += "Deviance = %g\n" % (self.deviance.mean(),)
        s += "pD       = %g\n" % (self.pD,)
        s += "DIC      = %g\n" % (self.DIC,)
        return s


    burnin = property(fget=lambda self: self._burnin,
            fset=setburnin,
            doc="Number of initial samples to be discarded")
    thin   = property(fget=lambda self: self._thin,
            fset=setthin,
            doc="Take every thin-th sample only")
    samples = property(
            fget=lambda self: concatenate([x[self.burnin::self.thin] for x in self._samples],0),
            doc="Samples from the posterior")
    deviance = property(
            fget=lambda self: concatenate([x[self.burnin::self.thin] for x in self.D]),
            doc="Deviances of the samples")
    mean_est = property(fget=lambda self: self.samples.mean(0),
            doc="Mean of the posterior")
    map_est  = property(fget=lambda self: self._map,
            doc="Maximum of the posterior")
    med_est  = property(fget=lambda self: median(self.samples,0),
            doc="median of the posterior")
    DIC = property(fget=lambda self: 2*self.deviance.mean()+
            2*_bayessdt.loglikelihood(self.nlevels,self.data,self.mean_est,self.model),
            doc="Deviance information criterion. This is useful for model comparisons")
    pD  = property(fget=lambda self: self.deviance.mean()+
            2*_bayessdt.loglikelihood(self.nlevels,self.data,self.mean_est,self.model),
            doc="Effective number of parameters")

def prior2python(priorstring):
    """This function returns a python function that evaluates the density of the given prior"""
    fstring = r"-?\d+\.?\d*|-?\d*\.\d+|-?\d+e-?\d+"
    gausspattern = r"gauss\s*\(\s*(%s)\s*,\s*(%s)\s*\)" % (fstring,fstring)
    betapattern  = r"beta\s*\(\s*(%s)\s*,\s*(%s)\s*\)" % (fstring,fstring)
    gammapattern = r"gamma\s*\(\s*(%s)\s*\)" % (fstring,)
    improperpattern = r">(%s)|<(%s)" % (fstring, fstring)
    if re.search(gausspattern,priorstring):
        return stats.norm(*[float(x) \
                for x in re.search(gausspattern,priorstring).groups()])
    elif re.search(betapattern,priorstring):
        return stats.beta(*[float(x) \
                for x in re.search(betapattern,priorstring).groups()])
    elif re.search(gammapattern,priorstring):
        return stats.gamma(float(re.search(gammapattern,priorstring).group(1)))
    elif re.search(improperpattern,priorstring):
        # These patterns generate an inproper prior
        return ImproperPrior()
    else:
        raise ValueError, "Unknown prior spec %s" % (priorstring,)


def prior2C(priorstring):
    """This function returns a python function that evaluates the density of the given prior"""
    fstring = r"-?\d+\.?\d*|-?\d*\.\d+|-?\d+e-?\d+"
    gausspattern = r"gauss\s*\(\s*(%s)\s*,\s*(%s)\s*\)" % (fstring,fstring)
    betapattern  = r"beta\s*\(\s*(%s)\s*,\s*(%s)\s*\)" % (fstring,fstring)
    gammapattern = r"gamma\s*\(\s*(%s)\s*\)" % (fstring,)
    greaterpattern = r">(%s)" % (fstring,)
    lesspattern = r"<(%s)" % (fstring,)
    if re.search(gausspattern,priorstring):
        return [GAUSSPRIOR] + [float(x) \
                for x in re.search(gausspattern,priorstring).groups()]
    elif re.search(betapattern,priorstring):
        return [BETAPRIOR] + [float(x) \
                for x in re.search(betapattern,priorstring).groups()]
    elif re.search(gammapattern,priorstring):
        return [GAMMAPRIOR, float(re.search(gammapattern,priorstring).group(1))]
    elif re.search(greaterpattern,priorstring):
        return [GREATERPRIOR, float(re.search(greaterpattern,priorstring).group(1))]
    elif re.search(lesspattern,priorstring):
        return [LESSPRIOR, float(re.search(greaterpattern,priorstring).group(1))]
    else:
        raise ValueError, "Unknown prior spec %s" % (priorstring,)

class ImproperPrior:
    """An improper prior that basically just raises an exception if the user attempts to use its pdf"""
    def __init__(self):
        pass
    def pdf(self,x):
        raise ImproperPriorException,"Attempted to use the pdf of an improper prior"

class ImproperPriorException(Exception):
    def __init__(self,message):
        self.message = message
    def __str__(self):
        return self.message
