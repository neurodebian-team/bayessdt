#!/usr/bin/env python
#encoding: utf8
##
## bayessdt -- bayesian inference for rating based ROC curves
## Copyright (C) 2009  Ingo Fründ
##
## This library is free software; you can redistribute it and/or
## modify it under the terms of the GNU Lesser General Public
## License as published by the Free Software Foundation; either
## version 2 of the License, or (at your option) any later version.
##
## This library is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## Lesser General Public License for more details.
##
## You should have received a copy of the GNU Lesser General Public
## License along with this library; if not, write to the Free Software
## Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301
## USA
##

import numpy as N
from scipy import stats

__all__ = ["RatingExp","logisticcdf","extremecdf","gausscdf"]
__doc__ = """This file defines various 'forward models' for the decision
process under question. It is useful to generate data sets with known
parameters and to check the integrity of the estimates.
"""

def logisticcdf(x):
    """cdf of logistic function"""
    return 1./(1+N.exp(-x))

def extremecdf(x):
    """cdf of extreme value (gumbel) distribution"""
    return 1-N.exp(-N.exp(x))

def gausscdf(x):
    """cdf of gaussian distribution"""
    return stats.norm.cdf(x)

class RatingExp(object):
    def __init__(self,nlevels=4,d=1.,c=None,a=1,cdf=logisticcdf):
        """Create a rating experiment to draw samples from

        :Parameters:
            nlevels     number of confidence levels for confidence rating
            d           discriminablity of signal and noise
            c           response criteria
            a           attention parameter (between 0 and 1)
        """
        if nlevels==1:
            raise ValueError,"Number of criteria should be larger than 1."

        if c==None:
            if nlevels == 2:
                # Yes/No Task: Set the criterion half way between signal and noise
                c = [0.5*d]
            else:
                # Rating task with more than two responses: Criteria are distributed
                #      equally between signal and noise
                c = [k*float(d)/(nlevels-2) for k in xrange(nlevels-1)]

        if not len(c)+1 == nlevels:
            raise ValueError,"Number of criteria (%d) and number of levels (%d) do not fit." \
                    % (len(c),nlevels)

        self.nlevels = nlevels
        self.d = d
        self.c = c
        self.a = a
        self.cdf = cdf

    def sample(self,ntrials=1000,Ps=0.5):
        """Draw samples form the experiment

        :Parameters:
            ntrials     total number of trials to be simulated
            Ps          probability that a trial contains a signal

        :Output:
            a list with 2*nlevels entries. The first nlevels entries correspond
                to 'only noise' trials and the second nlevels correspond to the
                'signal + noise' trials
        """
        # How many trials per category
        nsignal = int(ntrials*Ps)
        nnoise  = ntrials-nsignal

        # lower criteria
        c0 = N.array([-1000]+list(self.c))
        # upper criteria
        c1 = N.array(list(self.c)+[1000])

        # RNG
        if self.cdf == logisticcdf:
            RNG = N.random.logistic
        elif self.cdf == extremecdf:
            RNG = N.random.gumbel
        elif self.cdf == gausscdf:
            RNG = N.random.randn
        else:
            raise ValueError,"Invalid cdf %s" % (str(self.cdf),)

        # Prepare signal trials
        rsignal = N.zeros(self.nlevels)
        for k in xrange(nsignal):
            lapse = N.random.uniform()<self.a
            x = RNG() + lapse*self.d
            r = 0
            for cc in c1:
                if cc < x:
                    r += 1
            rsignal[r] += 1
        rnoise = N.zeros(self.nlevels)
        for k in xrange(nnoise):
            x = RNG()
            r = 0
            for cc in c1:
                if cc < x:
                    r += 1
            rnoise[r] += 1
        return list(rsignal)+list(rnoise)
