#!/usr/bin/env python

from distutils.core import setup, Extension

bayessdt_module = Extension("bayessdt._bayessdt",
        sources=["src/bayessdt.cc","src/sdtmodels.cc","src/prior.cc","src/sampler.cc","src/rng.cc"],language="c++")

setup(name="bayessdt",
    version="0.0",
    author="Ingo Fruend",
    author_email = "ingo.fruend@googlemail.com",
    description="Bayesian inference for signal detection theory models",
    ext_modules = [bayessdt_module],
    py_modules = ['bayessdt.bayessdt',"bayessdt.simulation"],
    )
